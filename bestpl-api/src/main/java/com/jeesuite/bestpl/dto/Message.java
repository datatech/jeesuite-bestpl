package com.jeesuite.bestpl.dto;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable{

	private static final long serialVersionUID = 1L;

	 private Integer id;

	    /**
	     * 消息类型
	     */
	    private Integer msgType = 1;

	    /**
	     * 发送用户id
	     */
	    private Integer fromUid;

	    private String fromUname;

	    /**
	     * 接受用户id
	     */
	    private Integer toUid;

	    private String toUname;

	    /**
	     * 是否已读
	     */
	    private Boolean isRead;

	    /**
	     * 创建时间
	     */
	    private Date createdAt;

	    /**
	     * 内容
	     */
	    private String content;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getMsgType() {
			return msgType;
		}

		public void setMsgType(Integer msgType) {
			this.msgType = msgType;
		}

		public Integer getFromUid() {
			return fromUid;
		}

		public void setFromUid(Integer fromUid) {
			this.fromUid = fromUid;
		}

		public String getFromUname() {
			return fromUname;
		}

		public void setFromUname(String fromUname) {
			this.fromUname = fromUname;
		}

		public Integer getToUid() {
			return toUid;
		}

		public void setToUid(Integer toUid) {
			this.toUid = toUid;
		}

		public String getToUname() {
			return toUname;
		}

		public void setToUname(String toUname) {
			this.toUname = toUname;
		}

		public Boolean getIsRead() {
			return isRead;
		}

		public void setIsRead(Boolean isRead) {
			this.isRead = isRead;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}
	    
	    
}
