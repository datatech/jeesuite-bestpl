SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS `sc_user`;
CREATE TABLE `sc_user` (
  `user_id` int(10) unsigned NOT NULL COMMENT '主键ID',
  `user_type` int(1) DEFAULT 1 COMMENT '用户类型(1:普通用户,2:内部编辑)',
  `signature` varchar(128) DEFAULT NULL COMMENT '签名',
  `post_count` int(11) unsigned DEFAULT '0' COMMENT '内容数量',
  `comment_count` int(11) unsigned DEFAULT '0' COMMENT '评论数量',
  `points` int(11) unsigned DEFAULT '0' COMMENT '声望值',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息';


DROP TABLE IF EXISTS `sc_post_category`;
CREATE TABLE `sc_post_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(16) COMMENT '类别名称',
  `parent_id` int(10) COMMENT '父级id',
  `post_count` int(10) unsigned DEFAULT '0' COMMENT '帖子数',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='主题分类';


DROP TABLE IF EXISTS `sc_posts`;
CREATE TABLE `sc_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(10) COMMENT '语言id',
  `user_name` varchar(16) COMMENT '语言id',
  `title` varchar(100) DEFAULT NULL COMMENT '语言名称',
  `summary` text DEFAULT NULL COMMENT '摘要',
  `tags` varchar(100) DEFAULT NULL COMMENT '标签多个,隔开',
  `content` text DEFAULT NULL COMMENT '内容',
  `category_id` int(10) COMMENT '栏目id',
  `comment_count` int(10) DEFAULT '0' COMMENT '评论数',
  `view_count` int(10) DEFAULT '0' COMMENT '评论数',
  `is_recommend` tinyint(1) DEFAULT 0 COMMENT '是否推荐',
  `is_original` tinyint(1) DEFAULT '1' COMMENT '是否原创',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `seo_keywords` varchar(50) DEFAULT NULL,
  `seo_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='正文帖子';


DROP TABLE IF EXISTS `sc_comment`;
CREATE TABLE `sc_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `post_id` int(10) COMMENT '帖子id',
  `user_id` int(10) COMMENT '语言id',
  `user_name` varchar(16) COMMENT '语言id',
  `content` text DEFAULT NULL COMMENT '评论内容',
  `like_count` int(8) unsigned DEFAULT '0' COMMENT '点赞人数',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='评论';


DROP TABLE IF EXISTS `sc_tags`;
CREATE TABLE `sc_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tag_type` int(1) DEFAULT '1' COMMENT '标签类别(1:普通标签，2:代码标签(类名、函数等))',
  `name` varchar(256) COMMENT '标签名称',
  `refer_count` int(11) unsigned DEFAULT '0' COMMENT '引用次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='评论';


DROP TABLE IF EXISTS `sc_post_r_tag`;
CREATE TABLE `sc_post_r_tag` (
  `post_id` int(10) NOT NULL,
  `tag_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帖子与标签关系';


DROP TABLE IF EXISTS `sc_message`;
CREATE TABLE `sc_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `msg_type` int(1) DEFAULT 1 COMMENT '消息类型',
  `from_uid` int(10) COMMENT '发送用户id',
  `from_uname` varchar(32) DEFAULT NULL,
  `to_uid` int(10) COMMENT '接受用户id',
  `to_uname` varchar(32) DEFAULT NULL,
  `content` text DEFAULT NULL COMMENT '内容',
  `is_read` tinyint(1) DEFAULT 0 COMMENT '是否已读',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='通知消息';

INSERT INTO `sc_post_category` VALUES (1000,'前沿技术',0,0,NULL),(1001,'行业新闻',0,0,NULL),(1002,'智能硬件',0,0,NULL),(1003,'互联网',0,0,NULL),(1004,'科技圈',0,0,NULL),(1005,'杂七杂八',0,0,NULL);

SET FOREIGN_KEY_CHECKS = 1;
