package com.jeesuite.bestpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.AsyncEventBus;
import com.jeesuite.bestpl.event.RewardEventListener;
import com.jeesuite.bestpl.event.MessageSendEventListener;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;
import com.jeesuite.passport.client.filter.AuthCheckSpringMvcFilter;
import com.jeesuite.springboot.starter.cache.EnableJeesuiteCache;
import com.jeesuite.springboot.starter.kafka.EnableJeesuiteKafkaProducer;
import com.jeesuite.springboot.starter.scheduler.EnableJeesuiteSchedule;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableJeesuiteCache
@EnableJeesuiteSchedule
@EnableJeesuiteKafkaProducer
@ComponentScan(value = {"com.jeesuite.bestpl","com.jeesuite.springweb"})
@ImportResource(locations = {"dubbo/consumer.xml"})
//@EnableDiscoveryClient
@ServletComponentScan({"com.jeesuite.passport.client"})
public class Application {
	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).web(true).run(args);
	}
	
	@Bean
    RestTemplate restTemplate() {
		return new RestTemplate();
    }
	
	@Bean
	AuthCheckSpringMvcFilter authCheckSpringMvcFilter(){
		return new AuthCheckSpringMvcFilter();
	}
	
	@Bean
	AsyncEventBus asyncEventBus(TopicProducerSpringProvider topicProducerProvider){
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		AsyncEventBus eventBus = new AsyncEventBus(executorService);
		//
		eventBus.register(new MessageSendEventListener(topicProducerProvider));
		eventBus.register(new RewardEventListener(topicProducerProvider));
		return eventBus;
	}
  
}
